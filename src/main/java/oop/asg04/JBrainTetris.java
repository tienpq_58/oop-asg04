package oop.asg04;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.Toolkit;

public class JBrainTetris extends JTetris {

	private DefaultBrain defaultBrain;
	private JCheckBox brainMode;
	private int tempCount;
	private DefaultBrain.Move point;
	
	JBrainTetris(int pixels) {
		super(pixels);
		defaultBrain = new DefaultBrain();
		tempCount = count;
		point = null;
	}

	public JComponent createControlPanel() {
		JPanel panel = (JPanel) super.createControlPanel();
		panel.add(new JLabel("Brain:"));
		brainMode = new JCheckBox("Brain active");
		panel.add(brainMode);
		return panel;
	}
	
	public void tick(int verb) {
		if(brainMode.isSelected()==false)
			super.tick(verb);
		else{
			if(count != tempCount) {
				tempCount = count;
				board.undo();
				point = defaultBrain.bestMove(board, currentPiece, HEIGHT+TOP_SPACE, null);
			}			
			if(verb == DOWN){
				if(currentX!=point.x)
					super.tick(currentX > point.x ? LEFT : RIGHT);
				if(!currentPiece.equals(point.piece)) 
					super.tick(ROTATE);
				super.tick(verb);
			}
		}	
	}

	public static void main(String[] args) {
		JBrainTetris BrainTetris = new JBrainTetris(16);
		JFrame frame = JBrainTetris.createFrame(BrainTetris);
		frame.setVisible(true);
	}
}