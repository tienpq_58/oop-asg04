package oop.asg04;

// Board.java

/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
 
 Biểu diễn một bảng tetris - một mảng boolean 2 chiều 
 Cho phép đặt các mảnh tetris và xóa dòng.
 Có một tính năng "undo" cho phép client thêm và xóa các mảnh một cách hiệu quả
 Không vẽ hay dính líu gì đến các điểm ảnh. Thay vào đó, chỉ biểu diễn bảng hai chiều trừu tượng.
*/
public class Board	{
	// Some ivars are stubbed out for you:
	// một số biến thực thể được khai báo sẵn cho bạn:
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean DEBUG = true;
	boolean committed;
	private int maxHeight;
	private int[] widths,heights;
	private boolean[][] backupGrid;
	private int backupMaxHeight;
	private int[] backupWidths,backupHeights;
	
	// Here a few trivial methods are provided:
	// Một số phương thức đơn giản được cho sẵn:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	 
	 Tạo một bảng rỗng với kích thước cho sẵn tính theo khối
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		grid = new boolean[width][height];
		committed = true;
		maxHeight = 0;
		widths = new int[height];
		heights = new int[width];
		backupGrid = new boolean[width][height];
		backupMaxHeight = 0;
		backupWidths = new int[height];
		backupHeights = new int[width];
	}
	
	
	/**
	 Returns the width of the board in blocks.
	 Trả về chiều ngang bảng tính theo khối.
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	 Trả về chiều cao của bảng tính theo khối.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	 
	 Trả về chiều cao của cột cao nhất trong bảng
	 Đối với bảng rỗng, giá trị này bằng 0.
	*/
	public int getMaxHeight() {	 
		return maxHeight;
	}
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	 Kiểm tra dữ liệu của bảng xem có nhất quán không --
	 dùng cho việc soát lỗi.
	*/
	public void sanityCheck() {
		if (DEBUG) {
			int[] tempWidths = new int[height];
			int[] tempHeights = new int[width];
			int tempMaxHeight = 0;
			for(int i=0;i<height;i++){
				for(int j=0;j<width;j++){
					if(grid[j][i])
						tempWidths[i]++;
					if(grid[j][i] && i >= tempHeights[j])
						tempHeights[j] = i+1;
					if(grid[j][i] && i >= tempMaxHeight)
						tempMaxHeight = i+1;
				}
			}
			for(int i=0;i<height;i++){
				if(widths[i]!=tempWidths[i]) throw new RuntimeException("description");
			}
			for(int i=0;i<width;i++){
				if(heights[i]!=tempHeights[i]) throw new RuntimeException("description");
			}
			if(maxHeight!=tempMaxHeight) throw new RuntimeException("description");
		}
	}
	
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 Cho một mảnh và giá trị x, trả về giá trị y mà
	 mảnh sẽ chạm đáy tại đó nếu nó được thả thẳng xuống
	 tại cột x
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	 Cài đặt: dùng skirt và mảng heights của các cột để tính nhanh kết quả -- thuật toán O(kích thước của skirt)
	*/
	public int dropHeight(Piece piece, int x) {
		int []skirt = piece.getSkirt();
		int temp = getColumnHeight(x)-skirt[0];
		for(int i=0;i<piece.getWidth();i++,x++){
			if((getColumnHeight(x)-skirt[i])>temp)
				temp = getColumnHeight(x)-skirt[i];
		}
		return temp;
	}
	
	
	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	 Trả về chiều cao của cột x -- 
	 nghĩa là giá trị y của khối cao nhất + 1.
	 Chiều cao bằng 0 nếu cột không chứa khối nào
	*/
	public int getColumnHeight(int x) {
		return heights[x];
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	 trả về số khối đã lấp trong hàng y
	*/
	public int getRowWidth(int y) {
		return widths[y];
	}
		
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	 Trả về true nếu khối (x,y) trong bảng đã được lấp.
	 Các khối nằm ngoài phạm vi hợp lệ của bảng luôn trả về true.
	*/
	public boolean getGrid(int x, int y) {
		if(x>=0 && x<width && y>=0 && y < height && grid[x][y] == false)
			return false;
		else
			return true;
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 Thử đặt thân của một mảnh vào bảng tetris
	 Chép các khối của mảnh vào lưới grid của bảng.
	 Trả về PLACE_OK nếu đặt được bình thường, hoặc
	 PLACE_ROW_FILLED nếu đặt bình thường và có ít nhất một dòng được lấp đầy.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	 Các trường hợp lỗi:
	 1, một phần của mảnh nằm ngoài bảng - trả về PLACE_OUT_BOUNDS
	 2. một phần của mảnh trùng với khối đã lấp trong bảng - trả về PLACE_BAD
	 Trong cả hai trường hợp lỗi, bảng bị đặt vào tình trạng không hợp lệ, client có thể gọi undo() để quay lại trạng thái hợp lệ trước đó.
	*/
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		// ném ngoại lệ nếu place() được gọi 
		// khi bảng chưa được commit (committed == false)
		if (!committed) throw new RuntimeException("place commit problem");	
		int result = PLACE_OK;
		backup();
		//backup(this,this.clone);
		int[] tempWidths = new int[height];
		int[] tempHeights = new int[width];
		System.arraycopy(widths, 0, tempWidths, 0, maxHeight);
		System.arraycopy(heights, 0, tempHeights, 0, width);
		int tempMaxHeight = maxHeight;
		TPoint []temp = piece.getBody();
		for(int i=0;i<temp.length;i++){
			int tempx = temp[i].x + x;
			int tempy = temp[i].y + y;
			if(tempx<0 || tempx>=width || tempy<0 || tempy >=height)
				return  PLACE_OUT_BOUNDS;
			if(getGrid(tempx, tempy)==true)
				return PLACE_BAD;
			grid[tempx][tempy]=true;
			tempWidths[tempy]++;
			if(tempy >= tempHeights[tempx])
				tempHeights[tempx] = tempy+1;
			if(tempy >= tempMaxHeight)
				tempMaxHeight = tempy+1;
			if(tempWidths[tempy]==width)
				result = PLACE_ROW_FILLED;
		}
		System.arraycopy(tempWidths, 0, widths, 0, tempMaxHeight);
		System.arraycopy(tempHeights, 0, heights, 0, width);
		maxHeight = tempMaxHeight;
		committed = false;
		try{
			sanityCheck();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return result;
	}
	
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	 Xóa các dòng đã được lấp đầy, dịch các khối bên trên xuống.
	 Trả về số dòng đã xóa.
	*/
	public int clearRows() {
		int rowsCleared = 0;
		for(int i=0;i<getMaxHeight();i++){
			while(getRowWidth(i)==width){
				rowsCleared++;
				for(int j=i;j<getMaxHeight()-1;j++){
					for(int k=0;k<width;k++)
						grid[k][j]=grid[k][j+1];
					widths[j]=widths[j+1];
				}
				for(int k=0;k<width;k++){
					grid[k][getMaxHeight()-1]=false;
				}
				widths[getMaxHeight()-1]=0;
				maxHeight--;
				for(int j=0;j<width;j++){
					if(heights[j]>i+1)
						heights[j]--;
					else{
						int k;
						for(k=i;k>=0;k--){
							if(grid[j][k]){
								heights[j]=k+1;
								break;
							}
						}
						if(k==-1)
							heights[j]=0;
					}
				}
			}
		}	
		committed = false;
		try{
			sanityCheck();
		}catch(Exception e){
			e.printStackTrace();
		}
		return rowsCleared;
	}
    //Gọi hàm backup trước khi gán committed = false để sao lưu
	private void backup(){
		for(int i=0;i<width;i++){
			System.arraycopy(grid[i], 0, backupGrid[i], 0, height);
		}
		backupMaxHeight = maxHeight;
		System.arraycopy(widths, 0, backupWidths, 0, height);
		System.arraycopy(heights, 0, backupHeights, 0, width);
	}

	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	 
	 Quay lui bảng về trạng thái committed trước đó.
	 Xem mô tả chi tiết tại đề bài.
	*/
	public void undo() {
		for(int i=0;i<width;i++){
			System.arraycopy(backupGrid[i], 0, grid[i], 0, height);
		}
		maxHeight = backupMaxHeight;
		System.arraycopy(backupWidths, 0, widths, 0, height);
		System.arraycopy(backupHeights, 0, heights, 0, width);
		committed = true;
	}
	
	
	/**
	 Puts the board in the committed state.
	 Đặt bảng vào trạng thái đã được commit
	*/
	public void commit() {
		committed = true;
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex state change over time.
	 (provided debugging utility) 
	 
	 Biểu diễn trạng thái bảng dưới dạng một xâu kí tự có thể in ra được.
	 Đây là loại tiện ích in tình trạng đối tượng, nó giúp quan sát quá trình thay đổi trạng thái phức tạp theo thời gian.
	 (tiện ích soát lỗi cho sẵn)
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
}


