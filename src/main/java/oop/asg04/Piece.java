package oop.asg04;
// Piece.java
// dùng tham số -encode=UTF-8 khi dịch file chứa tiếng Việt

import java.util.*;

/**
 An immutable representation of a tetris piece in a particular rotation.
 Each piece is defined by the blocks that make up its body.
 Biểu diễn bất biến của một mảnh tetris tại một trạng thái xoay.
 Mỗi mảnh được định nghĩa bằng các khối tạo nên thân mảnh đó
 
 Typical client code looks like...
 Mã client điển hình trông như thế này...
 <pre>
 // Create piece from string - tạo mảnh từ một xâu kí tự
 Piece pyra = new Piece(PYRAMID_STR); 
 int width = pyra.getWidth();			// 3
 // get rotation, slow way - tính ra mảnh tiếp theo, cách chậm
 Piece pyra2 = pyramid.computeNextRotation(); 
 
 // the array of root pieces - mảng các mảnh nguyên gốc
 Piece[] pieces = Piece.getPieces();	
 Piece stick = pieces[STICK];
 int width = stick.getWidth();		// get its width
 // get the next rotation, fast way - tính rotation tiếp theo, cách nhanh
 Piece stick2 = stick.fastRotation();	
 </pre>
*/
public class Piece {
	// Starter code specs out a few basic things, leaving
	// the algorithms to be done.
	// Mã khởi đầu đặc tả một số thứ cơ bản, chưa có thuật toán.
	private TPoint[] body;
	private int[] skirt;
	private int width;
	private int height;
	private Piece next; // "next" rotation - trạng thái xoay tiếp theo nếu xoay ngược chiều kim đồng hồ

	// singleton static array of first rotations
	// mảng static singleton gồm các trạng thái đầu tiên.
	static private Piece[] pieces;	

	/**
	 Defines a new piece given a TPoint[] array of its body.
	 Makes its own copy of the array and the TPoints inside it.
	 Khởi tạo một mảnh mới từ một mảng TPoint[] chứa thân mảnh.
	 Chép mảng points vào mảng của riêng đối tượng hiện hành.
	*/
	public Piece(TPoint[] points) {
		body  = points;
		width = height = 1;
		for(int i=0; i < body.length;i++){
			if(body[i].x+1 > width)
				width = body[i].x+1;
			if(body[i].y+1 > height)
				height = body[i].y+1;
		}
		skirt = new int[width];
		for(int i=0;i<width;i++){
			skirt[i] = body.length-1;
			for(int j=0;j<body.length;j++){
				if(body[j].x==i && body[j].y<skirt[i])
					skirt[i] = body[j].y;
			}	
		}
	}
	
	/**
	 * Alternate constructor, takes a String with the x,y body points
	 * all separated by spaces, such as "0 0  1 0  2 0	1 1".
	 * (provided)
	 * Hàm tạo thay thế, lấy đối số là một String với tọa độ 
	 * của các khối thân mảnh. Đã viết sẵn.
	 */
	public Piece(String points) {
		this(parsePoints(points));
	}

	/**
	 Returns the width of the piece measured in blocks.
	 Trả về chiều ngang của mảnh, tính theo số khối.
	*/
	public int getWidth() {
		return width;
	}

	/**
	 Returns the height of the piece measured in blocks.
	 Trả về chiều cao của mảnh, tính theo số khối
	*/
	public int getHeight() {
		return height;
	}

	/**
	 Returns a pointer to the piece's body. The caller
	 should not modify this array.
	 Trả về một con trỏ tới thân mảnh. Nơi gọi phương thức
	 không nên sửa mảng này.
	*/
	public TPoint[] getBody() {
		return body;
	}

	/**
	 Returns a pointer to the piece's skirt. For each x value
	 across the piece, the skirt gives the lowest y value in the body.
	 This is useful for computing where the piece will land.
	 The caller should not modify this array.
	 Trả về một con trỏ tới skirt của mảnh. Với mỗi giá trị x dọc theo chiều ngang của mảnh, skirt lưu giá trị y thấp nhất của thân mảnh.
	 Dữ liệu này hữu ích cho việc tính xem mảnh sẽ chạm đáy ở đâu. Nơi gọi hàm không nên sửa mảng này.
	*/
	public int[] getSkirt() {
		return skirt;
	}

	
	/**
	 Returns a new piece that is 90 degrees counter-clockwise
	 rotated from the receiver.
	 Trả về một mảnh mới là kết quả của việc xoay đối tượng hiện hành 90o ngược chiều kim đồng hồ
	 */
	public Piece computeNextRotation() {
		//giải thuật : tạo ra 1 ma  trận có kích thước = height*width a[i][j] trong đó i,j lưu tọa độ của điểm . 
		// Coi thân mảnh là 1 khối hình chữ nhật , với các điểm rỗng thì lưu a[i][j] = 0 , các điểm còn lại lưu a[i][j] = 1.
		// Dùng thuật toán xoay ma trận 90 độ ngược kim đồng hồ
		//Lưu ngược ma trận ra string rồi khởi tạo
		int [][]a = new int[width][height];
		for(int i=0;i<body.length;i++){
			a[body[i].x][body[i].y] = 1;
		}
		int [][]b = new int [height][width];
		for(int i=0;i<width;i++){
			for(int j=0;j<height;j++){
				b[height-j-1][i] = a[i][j];
			}
		}
		TPoint [] temp = new TPoint[4];
		int k = 0;
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				if(b[i][j]==1){
					temp[k] = new TPoint(i,j);
					k++;
				}
			}
		}
		return new Piece(temp);
	}

	/**
	 Returns a pre-computed piece that is 90 degrees counter-clockwise rotated from the receiver.	 Fast because the piece is pre-computed.
	 This only works on pieces set up by makeFastRotations(), and otherwise just returns null.
	 Trả về mảnh đã được tính từ trước, là kết quả của việc xoay đối tượng hiện hành 90o ngược chiều kim đồng hồ. Hàm này nhanh vì mảnh đã được tạo từ trước.
	 Hàm này chỉ chạy được đối với các mảnh đã được thiết lập bởi hàm makeFastRotations(), nếu không, nó sẽ chỉ trả về null.
	*/	
	public Piece fastRotation() {
		return next;
	}
	


	/**
	 Returns true if two pieces are the same --
	 their bodies contain the same points.
	 Interestingly, this is not the same as having exactly the
	 same body arrays, since the points may not be
	 in the same order in the bodies. Used internally to detect
	 if two rotations are effectively the same.
	 
	 Trả về true nếu hai mảnh giống nhau -- thân mảnh chứa các điểm giống nhau.
	 Lưu ý rằng đây không phải là hai mảnh có mảng thân mảnh giống nhau, vì thứ tự các điểm ở hai thân mảnh giống nhai có thể khác nhau.
	 Hàm này dùng nội bộ để kiểm tra xem hai rotation có tương đương hay không.
	*/
	public boolean equals(Object obj) {
		// standard equals() technique 1
		if (obj == this) return true;
		
		// standard equals() technique 2
		// (null will be false)
		if (!(obj instanceof Piece)) return false;
		Piece other = (Piece)obj;
		if(body.length !=  other.body.length)
			return false;
		else{
			for(int i=0;i<body.length;i++){
				int temp=0;
				for(int j=0;j<other.body.length;j++){
					if(body[i].equals(other.body[j])==true)
						break;
					temp++;
				}
				if(temp==other.body.length)
					return false;
			}
			return true;
		}
	}


	// String constants for the standard 7 tetris pieces
	// Các hằng xâu kí tự cho 7 mảnh tetris chuẩn
	public static final String STICK_STR	= "0 0	0 1	 0 2  0 3";
	public static final String L1_STR		= "0 0	0 1	 0 2  1 0";
	public static final String L2_STR		= "0 0	1 0 1 1	 1 2";
	public static final String S1_STR		= "0 0	1 0	 1 1  2 1";
	public static final String S2_STR		= "0 1	1 1  1 0  2 0";
	public static final String SQUARE_STR	= "0 0  0 1  1 0  1 1";
	public static final String PYRAMID_STR	= "0 0  1 0  1 1  2 0";
	
	// Indexes for the standard 7 pieces in the pieces array
	// Chỉ số của 7 mảnh chuẩn trong mảng pieces
	public static final int STICK = 0;
	public static final int L1	  = 1;
	public static final int L2	  = 2;
	public static final int S1	  = 3;
	public static final int S2	  = 4;
	public static final int SQUARE	= 5;
	public static final int PYRAMID = 6;
	
	/**
	 Returns an array containing the first rotation of
	 each of the 7 standard tetris pieces in the order
	 STICK, L1, L2, S1, S2, SQUARE, PYRAMID.
	 The next (counterclockwise) rotation can be obtained
	 from each piece with the {@link #fastRotation()} message.
	 In this way, the client can iterate through all the rotations
	 until eventually getting back to the first rotation.
	 (provided code)
	 
	 Trả về một mảng chứa trạng thái xoay đầu tiên của 7 mảnh chuẩn theo thứ tự STICK, L1, L2, S1, S2, SQUARE, PYRAMID.
	 Có thể lấy trạng thái tiếp theo (ngược chiều kim đồng hồ) của mỗi mảnh bằng cách gọi phương thức fastRotation(). Bằng cách đó, client có thể duyệt tuần tự qua tất cả các trạng thái cho đến khi quay lại trạng thái đầu tiên. Đã viết sẵn.
	*/
	public static Piece[] getPieces() {
		// lazy evaluation -- create static array if needed
		if (Piece.pieces==null) {
			// use makeFastRotations() to compute all the rotations for each piece
			Piece.pieces = new Piece[] {
				makeFastRotations(new Piece(STICK_STR)),
				makeFastRotations(new Piece(L1_STR)),
				makeFastRotations(new Piece(L2_STR)),
				makeFastRotations(new Piece(S1_STR)),
				makeFastRotations(new Piece(S2_STR)),
				makeFastRotations(new Piece(SQUARE_STR)),
				makeFastRotations(new Piece(PYRAMID_STR)),
			};
		}
		
		
		return Piece.pieces;
	}
	


	/**
	 Given the "first" root rotation of a piece, computes all
	 the other rotations and links them all together
	 in a circular list. The list loops back to the root as soon
	 as possible. Returns the root piece. fastRotation() relies on the
	 pointer structure setup here.
	 
	 Cho trước trạng thái gốc (đầu tiên) của một mảnh, tính tất cả các trạng thái xoay khác và liên kết chúng với nhau trong một danh sách dạng vành tròn quay ngược lại điểm gốc. Trả về mảnh gốc. fastRotation() phụ thuộc vào cấu trúc kết nối con trỏ được thiết lập tại đây.
	*/
	/*
	 Implementation: uses computeNextRotation()
	 and Piece.equals() to detect when the rotations have gotten us back to the first piece.
	 
	 Cài đặt: sử dụng computeNextRotation() và Piece.equals() để phát hiện khi các trạng thái xoay đã quay lại trạng thái gốc.
	*/
	private static Piece makeFastRotations(Piece root) {
		Piece temp = root;
		while(!root.equals(temp.computeNextRotation())){
			temp = connectionRotations(temp);
		}
		temp.next = root;
		return root;
	}

	private static Piece connectionRotations(Piece root){
		root.next = root.computeNextRotation();
		root=root.next;
		return root;
	}
	
	/**
	 Given a string of x,y pairs ("0 0	0 1 0 2 1 0"), parses
	 the points into a TPoint[] array.
	 (Provided code)
	 
	 Cho trước một xâu kí tự gồm các cặp x,y, ví dụ ("0 0	0 1 0 2 1 0"), dịch thành một mảng TPoint[].
	 Đã viết sẵn
	*/
	private static TPoint[] parsePoints(String string) {
		List<TPoint> points = new ArrayList<TPoint>();
		StringTokenizer tok = new StringTokenizer(string);
		try {
			while(tok.hasMoreTokens()) {
				int x = Integer.parseInt(tok.nextToken());
				int y = Integer.parseInt(tok.nextToken());
				
				points.add(new TPoint(x, y));
			}
		}
		catch (NumberFormatException e) {
			throw new RuntimeException("Could not parse x,y string:" + string);
		}
		
		// Make an array out of the collection
		TPoint[] array = points.toArray(new TPoint[0]);
		return array;
	}

	


}
