package oop.asg04;
// Brain.java -- the interface for Tetris brains

public interface Brain {
    // Move is used as a struct to store a single Move
    // ("static" here means it does not have a pointer to an
    // enclosing Brain object, it's just in the Brain namespace.)
	// Move là cấu trúc lưu một nước đơn.
	// ("static" ở đây có nghĩa rằng nó không có một con trỏ 
	// tới một đối tượng Brain bọc ngoài, 
	// nó chỉ nằm trong không gian tên Brain.)
    public static class Move {
        public int x;
        public int y;
        public Piece piece;
        public double score;    // lower scores are better
    }
    
    /**
     Given a piece and a board, returns a move object that represents
     the best play for that piece, or returns null if no play is possible.
     The board should be in the committed state when this is called.
	 Cho một mảnh và một bảng, trả về một đối tượng move 
	 đại diện cho nước đi tốt nhất cho mảnh đó, 
	 hoặc trả về null nếu không thể chơi mảnh đó.
	 Bảng cần ở trạng thái committed khi gọi hàm này.
     
     limitHeight is the height of the lower part of the board that pieces
     must be inside when they land for the game to keep going
      -- typically 20 (i.e. board.getHeight() - 4)
     If the passed in move is non-null, it is used to hold the result
     (just to save the memory allocation).
    */
    public Brain.Move bestMove(Board board, Piece piece, int limitHeight, Brain.Move move);
}
